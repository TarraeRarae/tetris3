﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetr : MonoBehaviour
{
    float fall = 0;
    public float fallspeed = 1;

    public bool allowRotation = true;
    public bool limitRotation = false;

    void Start()
    {
        
    }

    // Update is called once p er frame
    void Update()
    {
        CheckUserInput();
    }

    void CheckUserInput()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            transform.position += new Vector3(1, 0, 0);
            if (CheckPosition())
            {
                FindObjectOfType<Game>().UpdateGrid(this);
            }
            else
            {
                transform.position += new Vector3(-1, 0, 0);
            }
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.position += new Vector3(-1, 0, 0);
            if (CheckPosition())
            {
                FindObjectOfType<Game>().UpdateGrid(this);
            }
            else
            {
                transform.position += new Vector3(1, 0, 0);
            }
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (allowRotation)
            {
                if (limitRotation)
                {
                    if (transform.rotation.eulerAngles.z >= 90)
                    {
                        transform.Rotate(0, 0, -90);
                    }
                    else
                    {
                        transform.Rotate(0, 0, 90);
                    }
                }
                else
                {
                    transform.Rotate(0, 0, 90);
                }
                if (CheckPosition())
                {
                    FindObjectOfType<Game>().UpdateGrid(this);
                }
                else
                {
                    if (limitRotation)
                    {
                        if (transform.rotation.eulerAngles.z >= 90)
                        {
                            transform.Rotate(0, 0, -90);
                        }
                        else
                        {
                            transform.Rotate(0, 0, 90);
                        }
                    }
                    else
                    {
                        transform.Rotate(0, 0, 90);
                    }
                }
            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Time.time - fall >= fallspeed)
        {
            transform.position += new Vector3(0, -1, 0);
            if (CheckPosition())
            {
                FindObjectOfType<Game>().UpdateGrid(this); 
            }
            else
            {
                if(FindObjectOfType<Game>().CheckIsAboveGrid(this))
                {
                    FindObjectOfType<Game>().GameOver();
                }
                FindObjectOfType<Game>().DeleteRow();
                transform.position += new Vector3(0, 1, 0);
                enabled = false;
                FindObjectOfType<Game>().SpawnNextBl();
            }
            fall = Time.time;
        }

    }

    bool CheckPosition()
    {
        foreach (Transform tetri in transform)
        {
            Vector3 pos = FindObjectOfType<Game>().Round(tetri.position);
            if (FindObjectOfType<Game>().CheckInsideGrid(pos) == false) 
            {
                return false;
            }
            if (FindObjectOfType<Game>().GetTransformAtGridPosition(pos) != null && FindObjectOfType<Game>().GetTransformAtGridPosition(pos).parent != transform)
            {
                return false;
            }
        }
        return true;
    }
}
