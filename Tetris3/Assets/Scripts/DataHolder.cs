﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.IO;

public struct DataHolder
{
    public static int ValueOfScores;
    public static int BestScores;
    static string Path = @"Assets/Resources/Doc1.txt";
    
    public static void WriteToFile ()
    {
       using (StreamWriter sw = new StreamWriter(Path, false, System.Text.Encoding.Default))
        {
            sw.WriteLine(BestScores.ToString());
        }

    }

    public static void ReadFromFile()
    {

        using (StreamReader sr = new StreamReader(Path))
        {
            BestScores = Convert.ToInt32(sr.ReadToEnd());
        }
    }
}
