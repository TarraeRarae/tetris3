﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class Game : MonoBehaviour
{
    public static int gridWidth = 10;
    public static int gridHeight = 20;

    public static int scoreOneLine = 100;
    public static int scoreTwoLines = 300;
    public static int scoreThreeLines = 500;
    public static int scoreFourLines = 1200;

    private int numberOfRowInTurn = 0;
    public static int Currentscore = 0;

    public Text scores;
    public Text bestScores;

    public static Transform[,] grid = new Transform[gridWidth, gridHeight];

    void Start()
    {
        SpawnNextBl();
        DataHolder.ReadFromFile();
    
    }

    private void Update()
    {
        UpdateScore();
        UpdateUI();
        BestScr();
    }

    public void UpdateUI ()
    {
        scores.text = Currentscore.ToString();
        DataHolder.ValueOfScores = Convert.ToInt16(scores.text);
        if (DataHolder.ValueOfScores > DataHolder.BestScores) 
        {
            DataHolder.BestScores = DataHolder.ValueOfScores;
            DataHolder.WriteToFile();
        }
    }

    public void BestScr ()
    {
        bestScores.text = DataHolder.BestScores.ToString();
    }

    public void UpdateScore()
    {
       if (numberOfRowInTurn > 0)
        {
            if (numberOfRowInTurn == 1)
            {
                ClearedScoreOneLine();

            } else if (numberOfRowInTurn == 2)
            {
                ClearedScoreTwoLines();

            } else if (numberOfRowInTurn == 3)
            {
                ClearedScoreThreeLines();

            } else if (numberOfRowInTurn == 4)
            {
                ClearedScoreFourLines();

            }
            numberOfRowInTurn = 0;
        }
    }

    public void ClearedScoreOneLine()
    {
        Currentscore += scoreOneLine;
    }

    public void ClearedScoreTwoLines()
    {
        Currentscore += scoreTwoLines;
    }

    public void ClearedScoreThreeLines()
    {
        Currentscore += scoreThreeLines;
    }

    public void ClearedScoreFourLines()
    {
        Currentscore += scoreFourLines;
    }

    public bool CheckIsAboveGrid(Tetr tetr)
    {
        for (int x = 0; x < gridWidth; x++)
        {
            foreach (Transform mino in tetr.transform)
            {
                Vector2 pos = Round(mino.position);
                if (pos.y > gridHeight - 1)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public bool IsRowFull (int y)
    {
        for (int x = 0; x < gridWidth; x++ )
        {
            if (grid[x, y] == null)
            {
                return false;
            }
        }

        numberOfRowInTurn++;
        return true;
    }

    public void DeleteMino (int y)
    {
        for (int x = 0; x < gridWidth; x++)
        {
            Destroy (grid[x,y].gameObject);
            grid[x, y] = null;
        }
    }

    public void MoveRowDown (int y)
    {
        for (int x = 0; x < gridWidth; x++)
        {
            if (grid[x,y] != null) { 

                    grid[x, y - 1] = grid[x, y];
                    grid[x, y] = null;
                    grid[x, y - 1].position += new Vector3(0, -1, 0);

            }
        }
    }

    public void MoveAllRowsDown (int y)
    {
        for (int i = y; i < gridHeight; i++)
            MoveRowDown(i);
    }

    public void DeleteRow () {
        for (int y = 0; y < gridHeight; y++)
        {
            if(IsRowFull(y))
            {
                DeleteMino(y);
                MoveAllRowsDown(y + 1);
                y--;
            }
        }
    }

    public void UpdateGrid(Tetr tetr)
    {
        for (int y = 0; y < gridHeight; y++)
        {
            for(int x = 0; x < gridWidth; x++)
            {
                if (grid[x,y] != null )
                {
                    if (grid[x,y].parent == tetr.transform)
                    {
                        grid[x, y] = null;
                    }

                }
            }
        }
        foreach (Transform mino in tetr.transform)
        {
            Vector2 pos = Round(mino.position);
            if (pos.y < gridHeight)
            {
                grid[(int)pos.x, (int)pos.y] = mino;
            }
        }
    }

    public Transform GetTransformAtGridPosition (Vector2 pos)
    {  
        if (pos.y > gridHeight-1)
        {
            return null;

        } else
        {
            return grid[(int)pos.x, (int)pos.y];
        }

    }

    public bool CheckInsideGrid(Vector3 pos)
    {

        return ((int)pos.x >= 0 && (int)pos.x < gridWidth && (int)pos.y > 0);
    }

    public Vector3 Round(Vector3 pos)
    {
        return new Vector3(Mathf.Round(pos.x), Mathf.Round(pos.y));
    }

    public void SpawnNextBl ()
    {
        GameObject nextBl = (GameObject)Instantiate(Resources.Load(GetRandomBl(), typeof(GameObject)), new Vector2(5.0f, 20.0f), Quaternion.identity);
    }

    public string GetRandomBl ()
    {
        int randomBl = UnityEngine.Random.Range(1,8);
        string randomBlName = "Prefabs/tetris-J";
        switch (randomBl)
        {
            case 1:
                {
                    randomBlName = "Prefabs/tetris-T";
                    break;
                };
            case 2:
                {
                    randomBlName = "Prefabs/tetris-L";
                    break;
                }
            case 3:
                {
                    randomBlName = "Prefabs/tetris-Z";
                    break;
                }
            case 4:
                {
                    randomBlName = "Prefabs/tetris-S";
                    break;
                }
            case 5:
                {
                    randomBlName = "Prefabs/tetris-J";
                    break;
                }
            case 6:
                {
                    randomBlName = "Prefabs/tetris-long";
                    break;
                }
            case 7:
                {
                    randomBlName = "Prefabs/square";
                    break;
                }
        };

        return randomBlName;
    } 

    public void GameOver() => Application.LoadLevel("GameOver");
}
