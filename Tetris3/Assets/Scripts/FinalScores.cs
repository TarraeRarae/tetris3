﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalScores : MonoBehaviour
{
    public Text ResultScores;

    private void Start()
    {
        ResultScores.text = DataHolder.ValueOfScores.ToString();
    }

}
